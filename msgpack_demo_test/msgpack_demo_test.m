//
//  msgpack_demo_test.m
//  msgpack_demo_test
//
//  Created by ka2n on 2013/02/23.
//  Copyright (c) 2013年 Katsuma Ito. All rights reserved.
//

#import "msgpack_demo_test.h"
#import "MessagePack.h"
#import "MsgSerializer.h"

@implementation msgpack_demo_test
{
    NSData *_data;
    NSData *_jsonData;
    NSData *_msgpackData;
}

- (void)setUp
{
    [super setUp];

    NSArray *dataArr = @[@{
    @"key1": @"value",
    @"key2": @2,
    @"key3": @3.01,
    @"key4": @{
    @"nested": @"value"
    }
    }];

    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dataArr];
    _data = data;
    _jsonData = [NSJSONSerialization dataWithJSONObject:dataArr options:0 error:nil];
    _msgpackData = [dataArr messagePack];

    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down    code here.

    [super tearDown];
}


- (void)testUnSerializer
{
    MsgSerializer *serializer = [[MsgSerializer alloc] init];
    [serializer setType:SERIALIZER_JSON];
    id data = [serializer unserialize:_jsonData];
    
    
    [serializer setType:SERIALIZER_MSGPACK];
    id d = [serializer unserialize:_msgpackData];
    
}

- (void)testSerializer
{

}

@end
