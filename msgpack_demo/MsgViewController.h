//
//  MsgViewController.h
//  msgpack_demo
//
//  Created by Katsuma Ito on 2013/02/22.
//  Copyright (c) 2013年 Katsuma Ito. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MsgViewController : UIViewController
<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
