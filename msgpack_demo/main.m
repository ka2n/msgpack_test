//
//  main.m
//  msgpack_demo
//
//  Created by Katsuma Ito on 2013/02/22.
//  Copyright (c) 2013年 Katsuma Ito. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MsgAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MsgAppDelegate class]));
    }
}
