//
//  MsgSerializer.m
//  msgpack_demo
//
//  Created by Katsuma Ito on 2013/02/22.
//  Copyright (c) 2013年 Katsuma Ito. All rights reserved.
//

#import "MsgSerializer.h"
#import "MessagePack.h"

@implementation MsgSerializer

- (NSData *)serialize:(id)data
{
    id serialized = nil;
    switch (_type) {
        case SERIALIZER_JSON:
            serialized = [NSJSONSerialization dataWithJSONObject:data options:0 error:nil];
            break;
        case SERIALIZER_MSGPACK:
            serialized = [MessagePackPacker pack:data];
            break;
        default:
            break;
    }

    return serialized;
}

- (id)unserialize:(NSData *)data
{
    id result;
    switch (_type) {
        case SERIALIZER_JSON:
            result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            break;
        case SERIALIZER_MSGPACK:
            result = [data messagePackParse];
            break;
        default:
            break;
    }

    return result;
}

@end
