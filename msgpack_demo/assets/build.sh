#!/bin/bash

rm ./build/*
coffee --output ./build/ ./source/coffee/
jade ./source/jade -O ./build/
cp -r ./source/js/* ./build/
