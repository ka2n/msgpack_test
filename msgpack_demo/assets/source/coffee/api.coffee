# Native Bridge API

api = do ->

  serializers = {
    JSON: 0,
    MSGPACK: 1
  }

  current_serializer = serializers.JSON
  commandQueue = []

  _selialize = (payload, method) ->
    switch method
      when serializers.JSON
        return JSON.stringify payload
      when serializers.MSGPACK
        return msgpack.pack payload
      else
        return ''

  _createExecFrame = ->
    bridge = document.createElement('iframe')
    bridge.style.display = 'none'
    document.documentElement.appendChild(bridge)
    return bridge

  _exec = (service, action, args) ->
      exec_iframe = exec_iframe || _createExecFrame()
      commandQueue.push [service, action, args]
      exec_iframe.src = "api://ready/#{current_serializer}"

  return {
    nativeFetchMessage: ->
      msgPack = ''+_selialize(commandQueue, current_serializer)
      commandQueue = [] # cleanup
      return msgPack

    pushImage: (img)->
      _exec('MsgViewController', 'pushImage', {'image': img})
  }

window.api = api
