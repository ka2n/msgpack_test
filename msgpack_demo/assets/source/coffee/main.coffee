# Main
#
$(->

  url_field = $('#url_field')
  load_button = $('#load_button')
  copy_button = $('#copy_button')
  canvas = $('#main_canvas')

  loadImageToCanvas = (url, canvas) ->
    img = new Image
    img.onload = ->
      context = canvas.getContext('2d')
      context.drawImage(img, 0, 0, canvas.width * img.width/img.height, canvas.height)
    img.src = url

  copyImageToNative = (canvas) ->
      dataBase64 = canvas.toDataURL('img/png')
      api.pushImage(dataBase64)

  load_button.on 'click', ->
    loadImageToCanvas(url_field.val(), canvas.get(0))

  copy_button.on 'click', ->
    copyImageToNative(canvas.get(0))

  null
)
