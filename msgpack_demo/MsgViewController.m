//
//  MsgViewController.m
//  msgpack_demo
//
//  Created by Katsuma Ito on 2013/02/22.
//  Copyright (c) 2013年 Katsuma Ito. All rights reserved.
//

#import "MsgViewController.h"
#import "MsgSerializer.h"

@interface MsgViewController ()

@end

@implementation MsgViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_webView setDelegate:self];


    NSString *path = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"html" inDirectory:@"build"];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:path]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)pushImage:(NSDictionary *)args
{
    NSString *base64Image = [args objectForKey:@"image"];
    NSURL *url = [NSURL URLWithString:base64Image];
    NSData *imageData = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:imageData];
    [self.imageView setImage:image];
}

#pragma mark - WebView delegate

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = [request URL];
    if ([[url scheme] isEqualToString:@"api"]) {
        NSString *serializer = [url pathComponents][1];
        MsgSerializerType type = (MsgSerializerType) [serializer intValue];
        
        NSArray *embedData = [self unpackArg:[self valueFromWebView:webView] type:type];
        
        NSString *action = embedData[0][1];
        NSDictionary *args = embedData[0][2];
        
        SEL method;
        method = NSSelectorFromString([NSString stringWithFormat:@"%@:", action]);
        if ([self respondsToSelector:method]) {
            [self performSelector:method withObject:args afterDelay:0.0f];
        }
        
    }
    return YES;
}

- (id)unpackArg:(NSData *)data type:(MsgSerializerType)type
{
    MsgSerializer *serializer = [[MsgSerializer alloc] init];
    [serializer setType:type];
    return [serializer unserialize:data];
}

-(NSData *)valueFromWebView: (UIWebView *)webView
{
    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"api.nativeFetchMessage()"];
    return [result dataUsingEncoding:NSUTF8StringEncoding];
}

@end
