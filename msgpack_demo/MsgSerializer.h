//
//  MsgSerializer.h
//  msgpack_demo
//
//  Created by Katsuma Ito on 2013/02/22.
//  Copyright (c) 2013年 Katsuma Ito. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    SERIALIZER_JSON = 0,
    SERIALIZER_MSGPACK = 1
} MsgSerializerType;

@interface MsgSerializer : NSObject

@property (nonatomic, assign) MsgSerializerType type;

- (NSData *)serialize:(id)data;
- (id)unserialize:(NSData *)data;
@end
